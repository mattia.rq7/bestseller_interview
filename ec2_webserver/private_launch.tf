resource "aws_security_group" "launch_in_out_rules" {
  name        = "launch_in_out_rules"
  description = "Allowed connections"
  vpc_id = aws_vpc.webserver_vpc.id

  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    security_groups = [ aws_security_group.elb_in_out_rules.id ]
  }

  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    security_groups = [ aws_security_group.elb_in_out_rules.id ]
  }
}

resource "tls_private_key" "ssh_auth_keys" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "aws_key_pair" "ssh_auth" {
  key_name   = "ssh_key_pair"
  public_key = "${tls_private_key.ssh_auth_keys.public_key_openssh}"
}

resource "aws_launch_configuration" "webserver-launch" {
    name_prefix = "webserver-launch"
    image_id = "${var.ami}"
    instance_type = "${var.instance_type}"
    key_name = aws_key_pair.ssh_auth.key_name
    iam_instance_profile = aws_iam_instance_profile.ec2_profile.name
    user_data = "${file("launch_config/install_webserver.sh")}"
    security_groups = [ aws_security_group.launch_in_out_rules.id ]
    lifecycle {
        create_before_destroy = true
    }
}

resource "aws_autoscaling_group" "cpu-monitor" {
    max_size = "3"
    min_size = "1"
    desired_capacity = "1"
    health_check_type  = "ELB"
    min_elb_capacity = "1"
    vpc_zone_identifier = [ aws_subnet.PrivateSubnetA.id, aws_subnet.PrivateSubnetB.id, aws_subnet.PrivateSubnetC.id]
    load_balancers = [ aws_elb.public_loadbalancer.id ]
    launch_configuration = aws_launch_configuration.webserver-launch.name
    lifecycle {
          create_before_destroy = true
    }

    tag {
      key                 = "Name"
      value               = "HelloWorld webserver"
      propagate_at_launch = true
    }
}

