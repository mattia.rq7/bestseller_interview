resource "aws_subnet" "PublicSubnetA" {
  vpc_id = "${aws_vpc.webserver_vpc.id}"
  cidr_block = "${var.public_subnet_a}"

 availability_zone = "${data.aws_availability_zones.available.names[0]}"
}

resource "aws_subnet" "PublicSubnetB" {
  vpc_id = "${aws_vpc.webserver_vpc.id}"
  cidr_block = "${var.public_subnet_b}"

 availability_zone = "${data.aws_availability_zones.available.names[1]}"
}

resource "aws_subnet" "PublicSubnetC" {
  vpc_id = "${aws_vpc.webserver_vpc.id}"
  cidr_block = "${var.public_subnet_c}"

 availability_zone = "${data.aws_availability_zones.available.names[2]}"
}

resource "aws_subnet" "PrivateSubnetA" {
  vpc_id = "${aws_vpc.webserver_vpc.id}"
  cidr_block = "${var.private_subnet_a}"

 availability_zone = "${data.aws_availability_zones.available.names[0]}"
}

resource "aws_subnet" "PrivateSubnetB" {
  vpc_id = "${aws_vpc.webserver_vpc.id}"
  cidr_block = "${var.private_subnet_b}"
  
 availability_zone = "${data.aws_availability_zones.available.names[1]}"
}

resource "aws_subnet" "PrivateSubnetC" {
  vpc_id = "${aws_vpc.webserver_vpc.id}"
  cidr_block = "${var.private_subnet_c}"
  availability_zone = "${data.aws_availability_zones.available.names[2]}"
}
