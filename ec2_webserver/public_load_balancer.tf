resource "aws_elb" "public_loadbalancer" {
  name               = "webserver-lb"

  security_groups = [ aws_security_group.elb_in_out_rules.id ]
  
  subnets            = [ aws_subnet.PublicSubnetA.id, aws_subnet.PublicSubnetB.id, aws_subnet.PublicSubnetC.id ]
  
  listener {
    instance_port      = 80
    instance_protocol  = "http"
    lb_port            = 80
    lb_protocol        = "http"
  }

  listener {
    instance_port      = 22
    instance_protocol  = "tcp"
    lb_port            = 22
    lb_protocol        = "tcp"
  }

  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    target              = "HTTP:80/"
    interval            = 30
  }

  cross_zone_load_balancing   = true
  idle_timeout                = 400

  tags = {
    Name = "Loadbalancer for EC2 webserver"
  }
}

resource "aws_security_group" "elb_in_out_rules" {
  name        = "elb_in_out_rules"
  description = "Allowed connections"
  vpc_id = aws_vpc.webserver_vpc.id

  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress { 
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress { 
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

}