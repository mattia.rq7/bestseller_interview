resource "aws_route_table_association" "PublicSubnetA" {
    subnet_id = "${aws_subnet.PublicSubnetA.id}"
    route_table_id = "${aws_route_table.public_route_a.id}"
}

resource "aws_route_table_association" "PublicSubnetB" {
    subnet_id = "${aws_subnet.PublicSubnetB.id}"
    route_table_id = "${aws_route_table.public_route_b.id}"
}

resource "aws_route_table_association" "PublicSubnetC" {
    subnet_id = "${aws_subnet.PublicSubnetC.id}"
    route_table_id = "${aws_route_table.public_route_c.id}"
}

resource "aws_route_table_association" "PrivateSubnetA" {
    subnet_id = "${aws_subnet.PrivateSubnetA.id}"
    route_table_id = "${aws_route_table.private_route_a.id}"
}

resource "aws_route_table_association" "PrivateSubnetB" {
    subnet_id = "${aws_subnet.PrivateSubnetB.id}"
    route_table_id = "${aws_route_table.private_route_b.id}"
}

resource "aws_route_table_association" "PrivateSubnetC" {
    subnet_id = "${aws_subnet.PrivateSubnetC.id}"
    route_table_id = "${aws_route_table.private_route_c.id}"
}
