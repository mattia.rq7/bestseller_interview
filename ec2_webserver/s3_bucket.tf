
resource "aws_s3_bucket" "test_bucket" {

  bucket = "ec2-webserver-bucket"

  acl    = "private" 

  tags = {
    Name        = "My bucket"
  }

}


resource "aws_s3_bucket_object" "object" {

  bucket = aws_s3_bucket.test_bucket.id

  key    = "launch.sh"

  acl    = "private"

  source = "launch_config/install_webserver.sh"

  etag = filemd5("launch_config/install_webserver.sh")

}