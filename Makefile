TF_FILES_DIR=ec2_webserver
TF_CMD = cd $(TF_FILES_DIR) && terraform


.PHONY: check-credentials
check-credentials:

ifndef AWS_ACCESS_KEY_ID
	@echo "\n $(COLOR_ERR)Please export proper AWS credentials!$(COLOR_OFF)\n"
	@exit 1
endif
ifndef AWS_SECRET_KEY_ID
	@echo "\n $(COLOR_ERR)Please export proper AWS credentials!$(COLOR_OFF)\n"
	@exit 1
endif


.PHONY: checks
checks: check-credentials


.PHONY: preflight
preflight:					# Setup before running terraform
	@echo
	@$(MAKE) -s checks
	@$(TF_CMD) init
	@$(TF_CMD) get -update


.PHONY: plan
plan: preflight				##    Update modules and show plan
	@$(TF_CMD) plan
	

.PHONY: apply-force
apply-force: preflight		##   Update modules and apply plan
	@$(TF_CMD) apply -auto-approve=true


.PHONY: destroy-force
destroy-force: preflight	##  Update modules, show plan, destroy without confirmation
	@$(TF_CMD) plan -destroy
	@$(TF_CMD) apply -destroy -auto-approve=true

