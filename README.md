# HOW TO DEPLOY THE WEBSERVER

1. First you need to set (or export depending on your platform) the AWS related credential variables:

   - set AWS_ACCESS_KEY_ID=XXXXXXXXXXXXXXXX

   - set AWS_SECRET_KEY_ID=XXXXXXXXXXXXXXXX


2. From the main directory of this repository, i.e. the folder containing the Makefile, run respectively:

   - **make apply-force** to spin up the infrastructure
   
   - **make destroy-force** to destroy the infrastructure
   
The DNS address to be accessed to see the webpage will be displayed at the end of apply-force command.

